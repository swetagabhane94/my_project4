function filter(elements, cb) {
  let output = [];
  let newArr = [];
  if (elements.length == 0) {
    return output;
  }
  for (let index = 0; index < elements.length; index++) {
    newArr = cb(elements[index], index, elements);

    if (newArr === true) {
      output.push(elements[index]);
    }
  }
  return output;
}

module.exports = filter;
