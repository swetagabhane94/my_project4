const each = require("./each.cjs");
const items = [1, 2, 3, 4, 5, 5];
function multiplyBy8(element, index, elements) {
  return element * 8;
}

console.log(each(items, multiplyBy8));
