function flatten(elements, depth = 1) {
  if (depth === 0) {
    return elements;
  }
  let output = [];
  if (elements.length === 0) {
    return output;
  }
  for (let index = 0; index < elements.length; index++) {
    if (Array.isArray(elements[index]) && depth >= 1) {
      output = output.concat(flatten(elements[index], depth - 1));
    } else if (elements[index] === null || elements[index] === undefined) {
      continue;
    } else {
      output.push(elements[index]);
    }
  }
  return output;
}

module.exports = flatten;
