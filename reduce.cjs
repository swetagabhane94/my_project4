function reduce(elements, cb, startingValue) {
  if (elements.length == 0) {
    return [];
  }
  if (startingValue === undefined) {
    startingValue = elements[0];
  } else {
    startingValue = cb(startingValue, elements[0]);
  }
  for (let index = 1; index < elements.length; index++) {
    startingValue = cb(startingValue, elements[index], index, elements);
  }
  return startingValue;
}

module.exports = reduce;
