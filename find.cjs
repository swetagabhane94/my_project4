function find(elements, cb) {
  let output = [];
  if (elements.length == 0) {
    return output;
  }
  for (let index = 0; index < elements.length; index++) {
    output = cb(elements[index], cb, elements);

    if (output) {
      return elements[index];
    }
  }
}

module.exports = find;
