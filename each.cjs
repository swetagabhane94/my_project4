function each(elements, cb) {
  let output = [];
  if (elements.length == 0) {
    return output;
  }
  for (let index = 0; index < elements.length; index++) {
    output.push(cb(elements[index], index, elements));
  }
  return output;
}

module.exports = each;
