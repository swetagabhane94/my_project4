function map(elements, cb) {
  if (elements.length == 0) {
    return [];
  }
  let output = [];
  for (let index = 0; index < elements.length; index++) {
    output.push(cb(elements[index], index, elements));
  }
  return output;
}

module.exports = map;
